use std::fs::{ File, OpenOptions };
use std::io::{ ErrorKind, Error };
use std::io::{ BufReader, prelude::* };
use crate::config::CONFIG;
use crate::output::status_message;
use crate::input::prompt;


pub fn load_file(filename: &String) -> Result<(), Error> {
    let cfg = &mut CONFIG.lock().unwrap();
    let file = File::open(filename).unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(filename)
                .expect("Error creating a file")
        } else {
            panic!("Error loading a file");
        }
    });

    let reader = BufReader::new(file);

    if cfg.rows.len() != 0 {
        cfg.rows.clear();
    }

    for line in reader.lines() {
        let mut l = line?;
        l = l.replace("\t", "    ");
        cfg.rows.push(l);
    }

    cfg.filename = Some(filename.clone());

    Ok(())
}

pub fn save_file() -> Result<(), Error>{
    let cfg = CONFIG.lock().unwrap();
    let data = cfg.rows.join("\n");

    let mut filename = match &cfg.filename {
        Some(name) => name.clone(),
        None => "".to_string()
    };

    drop(cfg);

    if filename == "" {
        match prompt("Enter filename to save: ") {
            Some(name) => filename = name,
            None => return Err(Error::new(ErrorKind::InvalidInput, "Invalid filename"))
        }
    }
    
    let mut file = File::create(&filename)?;

    write!(file, "{}", data)?;

    CONFIG.lock().unwrap().filename = Some(filename);
    CONFIG.lock().unwrap().is_saved = true;

    status_message("File Saved!");

    Ok(())
}
