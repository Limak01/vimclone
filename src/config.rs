use std::sync::Mutex;
use lazy_static::lazy_static;
use crate::terminal::get_window_size;

lazy_static! {
    pub static ref CONFIG: Mutex<Config> = Mutex::new(Config::new());
}

pub enum Mode {
    Insert,
    Normal,
    Command
}

impl std::fmt::Display for Mode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Mode::Insert => write!(f, "{}", "INSERT"),
            Mode::Normal => write!(f, "{}", "NORMAL"),
            Mode::Command => write!(f, "{}", "COMMAND"),
        }
    }
}

pub struct Config {
    pub cursor_pos: (u16, u16),
    pub offset: u16,
    pub win_size: (u16, u16),
    pub rows: Vec<String>,
    pub filename: Option<String>,
    pub mode: Mode,
    pub status_message: Option<String>,
    pub is_saved: bool,
}

impl Config {
    pub fn new() -> Config {
        let mut win_size = get_window_size();
        
        //Make space for status bar and message bar
        win_size.1 -= 2;

        Config {
            cursor_pos: (1, 1),
            offset: 0,
            win_size,
            rows: Vec::new(),
            filename: None,
            status_message: None,
            mode: Mode::Normal,
            is_saved: true,
        }
    }
}


