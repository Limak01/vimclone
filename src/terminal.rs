use termios::*;
use crate::config::CONFIG;
use crate::input::Key;

pub fn into_raw_mode(original_term: &Termios) -> Result<(), std::io::Error> {
    let mut raw_term = original_term.clone();

    raw_term.c_iflag &= !(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw_term.c_oflag &= !(OPOST);
    raw_term.c_cflag |= CS8;
    raw_term.c_lflag &= !(ECHO | ICANON | IEXTEN | ISIG);
    raw_term.c_cc[VMIN] = 1;
    raw_term.c_cc[VTIME] = 0;
 
    tcsetattr(0, TCSAFLUSH, &mut raw_term)?; 

    Ok(())
}

pub fn get_window_size() -> (u16, u16) {
    let size: termsize::Size = match termsize::get() {
        Some(val) => val,
        None => panic!("Error getting terminal size"),
    };

    return (size.cols, size.rows);
}

pub fn scroll(cfg: &mut crate::config::Config) {
    if cfg.cursor_pos.0 > cfg.win_size.1 {
                cfg.offset += 1;
                cfg.cursor_pos.0 -= 1;
    }

    if cfg.cursor_pos.0 < 1 {
        cfg.offset -= 1;
        cfg.cursor_pos.0 += 1;
    }
}

pub fn move_cursor(key: Key) {
    let cfg = &mut CONFIG.lock().unwrap();

    let curr_row: usize = (cfg.cursor_pos.0 + cfg.offset) as usize;

    match key {
        Key::DOWN => {
            if cfg.cursor_pos.0 <= cfg.win_size.1 && curr_row < cfg.rows.len() {
                cfg.cursor_pos.0 += 1;

                if cfg.rows[curr_row].len() < cfg.cursor_pos.1 as usize {
                    cfg.cursor_pos.1 = cfg.rows[curr_row].len() as u16 + 1;
                }
            }
        }
        Key::UP => {
            if cfg.cursor_pos.0 > 0 && curr_row > 1 {
                cfg.cursor_pos.0 -= 1;

                if cfg.rows[curr_row - 2].len() < cfg.cursor_pos.1 as usize {
                    cfg.cursor_pos.1 = cfg.rows[curr_row - 2].len() as u16 + 1;
                }
            }
        }
        Key::RIGHT => {
            let row_size = cfg.rows[curr_row - 1].len();

            if cfg.cursor_pos.1 <= row_size as u16{
                cfg.cursor_pos.1 += 1;
            }
        }
        Key::LEFT => {
            if cfg.cursor_pos.1 - 1 != 0 {
                cfg.cursor_pos.1 -= 1;
            }
        }

        _ => ()
    }
}
