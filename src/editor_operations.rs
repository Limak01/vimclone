use crate::config::{ CONFIG, Mode };

pub fn change_mode(mode: Mode) {
    let cfg = &mut CONFIG.lock().unwrap();
    cfg.mode = mode;
}

pub fn insert_row(rows: &mut Vec<String>, at: usize) {
    let rows_len = rows.len();
    
    if (at == 0 || at > 0) && at < rows_len {
        rows.insert(at, "".to_string());
    } else {
        rows.push(String::from(""));
    }
}

fn delete_row(rows: &mut Vec<String>, at: usize) -> Option<String> {
    let rows_len = rows.len();
    
    if (at == 0 || at > 0) && at < rows_len {
        Some(rows.remove(at))
    } else {
        None
    }
}

pub fn insert_char(c: char) {
    let cfg = &mut CONFIG.lock().unwrap();

    let col_i: usize = cfg.cursor_pos.1.into();
    let row_i: usize = cfg.cursor_pos.0.into();

    if cfg.rows.len() == 0 { 
        insert_row(&mut cfg.rows, 0);
    }

    let row = &mut cfg.rows[row_i - 1];
    row.insert(col_i - 1, c);
    cfg.cursor_pos.1 += 1;
    cfg.is_saved = false;
}

pub fn delete_char() {
    let cfg = &mut CONFIG.lock().unwrap();

    let col_i: usize = cfg.cursor_pos.1.into();
    let row_i: usize = cfg.cursor_pos.0.into();

    if col_i - 1 != 0 {
        let row = &mut cfg.rows[row_i - 1];

        // Remove chare before cursor
        row.remove(col_i - 2);
        cfg.cursor_pos.1 -= 1;
    } else if row_i - 1 != 0 {
        if let Some(row) = delete_row(&mut cfg.rows, row_i - 1) {
            cfg.cursor_pos.1 =  cfg.rows[row_i - 2].len() as u16 + 1; 
            cfg.cursor_pos.0 -= 1;

            cfg.rows[row_i - 2].push_str(row.as_str()); 
        }
    }

    cfg.is_saved = false;
}

// Moves row to the next line starting from cursor position
pub fn move_to_next_line() {
    let cfg = &mut CONFIG.lock().unwrap();
     
    let row_i: usize = cfg.cursor_pos.0.into();
    let col_i: usize = cfg.cursor_pos.1.into();

    insert_row(&mut cfg.rows, row_i);

    let new_row: String = cfg.rows[row_i - 1].drain(col_i - 1..).collect();
    cfg.rows[row_i] = new_row;
    cfg.cursor_pos.0 += 1;
    cfg.cursor_pos.1 = 1;
}

pub fn to_end_of_line() {
    let cfg = &mut CONFIG.lock().unwrap();
    let row_i: usize = (cfg.cursor_pos.0 - 1).into();

    cfg.cursor_pos.1 = cfg.rows[row_i].len() as u16 + 1;

}

pub fn to_beginning_of_line() {
    let cfg = &mut CONFIG.lock().unwrap();
    cfg.cursor_pos.1 = 1;
}
