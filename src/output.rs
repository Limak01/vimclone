use crate::config;
use std::io::{Write, stdout};
use crate::terminal::scroll;


fn draw_rows(cfg: &config::Config) -> Result<(), std::io::Error> {
    for i in 0..cfg.win_size.1 {
        let curr_row = i + cfg.offset;

        if curr_row < cfg.rows.len() as u16 {
            write!(stdout(), "{}\r\n", cfg.rows[curr_row as usize])?;
        } else {
            write!(stdout(), "~\r\n")?;
        }
    }

    Ok(())
}

fn draw_status_bar(cfg: &config::Config) -> Result<(), std::io::Error> {
    let curr_file = match &cfg.filename {
        Some(name) => name.clone(),
        None => "[No Name]".to_string(),
    };

    let left_side = format!(" {} {}", cfg.mode, curr_file);
    let right_side = format!("{}:{} ", cfg.cursor_pos.0, cfg.cursor_pos.1);

    write!(stdout(), "\x1b[7m{}", left_side)?;
    for i in left_side.len() as u16 .. cfg.win_size.0 {
        if cfg.win_size.0 - i == right_side.len() as u16 {
            write!(stdout(), "{}", right_side)?;
            break;
        }
        write!(stdout(), " ")?;
    }
    write!(stdout(), "\x1b[0m" )?;

    Ok(())
}

pub fn status_message(msg: &str) {
    let cfg = &mut config::CONFIG.lock().unwrap();
    cfg.status_message = Some(msg.to_string());
}

fn draw_message_bar(cfg: &config::Config) -> Result<(), std::io::Error> {
    if let Some(message) = &cfg.status_message {
        write!(stdout(), "{}", message)?;
    }

    Ok(())
}

pub fn refresh_screen() -> Result<(), std::io::Error> {
    let cfg = &mut config::CONFIG.lock().unwrap();
    
    scroll(cfg);

    write!(stdout(), "\x1b[H\x1b[2J")?;

    draw_rows(&cfg)?;
    draw_status_bar(&cfg)?;
    draw_message_bar(&cfg)?;

    write!(stdout(), "\x1b[{};{}H", cfg.cursor_pos.0, cfg.cursor_pos.1)?;
    stdout().flush()?;

    Ok(())
}
