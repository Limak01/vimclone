use std::io::{ Read, stdin };
use termios::{ Termios, TCSANOW, tcsetattr };
use crate::terminal::move_cursor;
use crate::config::{ CONFIG, Mode };
use crate::editor_operations::{ change_mode, insert_char, delete_char, move_to_next_line, to_end_of_line, to_beginning_of_line };
use crate::fileio::save_file;
use crate::output::{ status_message, refresh_screen };

pub enum Key {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    CHAR(char),
    ESC,
    INSERT,
    ENTER,
    COMMAND,
    BACKSPACE,
    EOL, // End of line
    BOL, // Beginning of line
    NONE,
}

fn read_key() -> Result<Key, std::io::Error> {
    let cfg = CONFIG.lock().unwrap();

    let mut buff = [0; 5];
    let read_bytes = stdin().read(&mut buff)?;

    let key = buff[0] as char;

    match cfg.mode {
        Mode::Normal => {
            match key {
                'k' => Ok(Key::UP),
                'j' => Ok(Key::DOWN),
                'l' => Ok(Key::RIGHT),
                'h' => Ok(Key::LEFT),
                'i' => Ok(Key::INSERT),
                ':' => Ok(Key::COMMAND),
                '$' => Ok(Key::EOL),
                '0' => Ok(Key::BOL),
                _ => Ok(Key::NONE),
            }
        }
        Mode::Insert => {
            match key as u8 {
                27 => Ok(Key::ESC),
                127 => Ok(Key::BACKSPACE),
                13 => Ok(Key::ENTER),
                _ => Ok(Key::CHAR(key)),
            }
        }
        Mode::Command => {
            match key as u8 {
                27 => Ok(Key::ESC),
                13 => Ok(Key::ENTER),
                127 => Ok(Key::BACKSPACE),
                _ => Ok(Key::CHAR(key)),
            }
        }
    }
}

pub fn prompt(message: &str) -> Option<String> {
    let mut result = String::new();

    loop {
        status_message(format!("{}{}", message, result).as_str()); 

        refresh_screen().unwrap();
        let key = read_key().unwrap();

        match key {
            Key::CHAR(c) => result.push(c),
            Key::BACKSPACE => {
                result.pop().unwrap_or_default();
            },
            Key::ENTER => return Some(result),
            Key::ESC =>  return None ,
            _ => ()
        };
    }
}

fn handle_command(org_term: &Termios) -> Result<(), std::io::Error>{
    match prompt(":") {
        Some(command) => {
            match command.as_str() {
                "q" => {
                    let quit = || -> Result<(), std::io::Error> {
                        tcsetattr(0, TCSANOW, &org_term)?;
                        std::process::exit(0);
                    };

                    if !CONFIG.lock().unwrap().is_saved {
                        match prompt("Currently edited file is not saved. Continue? (y/n): ") {
                            Some(answer) => {
                                if answer == "y" {
                                    quit()?;
                                }
                            }
                            None => ()
                        }
                    } else {
                        quit()?;
                    }

                    status_message("");
                }
                "w" => {
                    match save_file() {
                        Err(error) => {
                            if error.kind() == std::io::ErrorKind::InvalidInput {
                                status_message(&error.to_string());
                            } else {
                                return Err(error);
                            }
                        }
                        _ => ()
                    };
                }
                _ => ()
            }
        }
        None => status_message("") 
    }

    change_mode(Mode::Normal);

    Ok(())
}

pub fn handle_keypress(org_term: &Termios) -> Result<(), std::io::Error> {
    let key = read_key()?;
    
    match key {
        Key::UP => move_cursor(Key::UP),
        Key::DOWN => move_cursor(Key::DOWN),
        Key::LEFT => move_cursor(Key::LEFT),
        Key::RIGHT => move_cursor(Key::RIGHT),
        Key::ESC => change_mode(Mode::Normal),
        Key::INSERT => change_mode(Mode::Insert),
        Key::EOL => to_end_of_line(),
        Key::BOL => to_beginning_of_line(),
        Key::BACKSPACE => delete_char(),
        Key::COMMAND => {
            change_mode(Mode::Command);
            handle_command(org_term)?;
        }
        Key::CHAR(c) => insert_char(c),
        Key::ENTER => move_to_next_line(),
        _ => ()
    }

    Ok(())
}
