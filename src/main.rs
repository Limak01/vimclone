pub mod terminal;
pub mod config;
pub mod input;
pub mod output;
pub mod fileio;
pub mod editor_operations;

use termios::Termios;
use std::io::{ Write, stdout };

fn main() -> Result<(), std::io::Error> {
    let original_term = Termios::from_fd(0)?;
    terminal::into_raw_mode(&original_term)?;
    let args: Vec<String> = std::env::args().collect();

    if args.len() >= 2 {
        fileio::load_file(&args[1])?;
    }

    stdout().flush()?;
    
    loop {
        output::refresh_screen()?;
        input::handle_keypress(&original_term)?;
    }
}
